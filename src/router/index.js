import Vue from 'vue'
import VueRouter from 'vue-router'
// import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('../views/Testhome/Home.vue')
  },
  {
    path: '/comment',
    name: 'comment',
    component: () => import('../views/Testhome/homeComment.vue')
  },
  {
    path: '/Formhome',
    name: 'formhome',
    component: () => import('../views/Testhome/comment.vue')
  },
  {
    path: '/about',
    name: 'About',
    component: () =>
      import('../views/heading/Heading.vue')
  },
  {
    path: '/heading',
    name: 'Heading',
    component: () => import('../views/heading/Heading.vue')
  },
  {
    path: '/room',
    name: '/Room',
    component: () => import('../views/Room.vue')
  },
  {
    path: '/contact',
    name: 'Contact',
    component: () => import('../views/Contact.vue')
  },
  {
    path: '/frommember',
    name: 'Fromcmember',
    component: () => import('../views/Frommember.vue')
  },
  {
    path: '/editprofile',
    name: '/Editprofile',
    component: () => import('../views/Testapply/Editprofile.vue')
  },

  {
    path: '/myheading',
    name: '/Myheading',
    component: () => import('../views/heading/Myheading.vue')
  },
  {
    path: '/login',
    name: '/Login',
    component: () => import('../views/Login.vue')
  },
  {
    path: '/addnews',
    name: '/Addheading_news',
    component: () => import('../views/heading/Add_news.vue')
  },
  {
    path: '/homeadmin',
    name: '/homeadmin',
    component: () => import('../Admin/Home_Statistic.vue')
  },
  {
    path: '/member',
    name: '/Member',
    component: () => import('../components/Member.vue')
  },
  {
    path: '/admin',
    name: '/Admin',
    component: () => import('../components/Admin.vue')
  },
  {
    path: '/statusHeading',
    name: '/StatusHeading',
    component: () => import('../Admin/Unheading/StatusHeading.vue')
  },
  {
    path: '/fromunproperly',
    name: '/Fromunproperly',
    component: () => import('../Admin/Unheading/Heading_Ad.vue')
  },
  {
    path: '/contactadmin',
    name: '/Contactadmin',
    component: () => import('../Admin/ContactadMin/Contactadmin.vue')
  },
  {
    path: '/Sendmass',
    name: '/sendmass',
    component: () => import('../Admin/ContactadMin/Contactsend.vue')
  },
  {
    path: '/fromcontact',
    name: '/Fromcontact',
    component: () => import('../Admin/ContactadMin/Fromcontact.vue')
  },
  {
    path: '/Testapply',
    name: '/testapply',
    component: () => import('../Admin/Namemember.vue')
  },
  {
    path: '/chart',
    name: 'chart',
    component: () => import('../Admin/Chart/pie.vue')
  },
  {
    path: '/bar',
    name: 'bar',
    component: () => import('../Admin/Chart/Bar.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router

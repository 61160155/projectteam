import { AUTH_LOGIN, AUTH_LOGOUT } from '../mutation-types'
import router from '../../router'
import axios from 'axios'
export default {
  namespaced: true,
  state: () => ({
    user: null
  }),
  mutations: {
    [AUTH_LOGIN] (state, payload) {
      state.user = payload
    },
    [AUTH_LOGOUT] (state) {
      state.user = null
    }
  },
  actions: {
    async login ({ commit }, payload) {
      console.log(payload)
      try {
        const res = await axios.post('http://localhost:8081/auth/login', { email: payload.email, password: payload.password })
        console.log(res.data)
        // const user = res.data.user
        // localStorage.setItem('user', JSON.stringify(user))
        router.push('/')
        commit(AUTH_LOGIN, res.data.apply)
      } catch (error) {
        console.log('Error')
      }
      const user = payload
      // if (payload.email === 'User01@user.com') {
      //   router.push('/')
      //   commit(AUTH_LOGIN, user)
      // }
      if (payload.email === 'Admin@gmail.com') {
        router.push('/homeadmin')
        commit(AUTH_LOGIN, user)
      }
    },
    logout ({ commit }) {
      commit(AUTH_LOGOUT)
    }
  },
  getters: {
    isLogin (state, getters) {
      return state.user != null
    }
  }
}
